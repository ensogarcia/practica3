package com.practica3.test.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.practica3.test.MyGdxGame;

public class FinalScreen extends BaseScreen {
    public FinalScreen(MyGdxGame game) {
        super(game);
    }
    private Stage stage;

    @Override
    public void show() {

        stage= new Stage();

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1f, 0.5f, 0.8f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            System.out.println("adsadsadsadsadsa");
            game.setScreen(new Menu(game));
        }
        //System.out.print(Gdx.input.getY());

        stage.act();
        stage.draw();
    }
    @Override
    public void hide() {
        stage.dispose();
    }

}
