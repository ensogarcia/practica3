package com.practica3.test.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.practica3.test.Hud;
import com.practica3.test.MyGdxGame;
import com.practica3.test.Player;

public class PlayScreen implements Screen {

    private MyGdxGame game;
    private OrthographicCamera camera;

    private Hud hud;
    private Player player;

    public Body playerBody;
    public BodyDef playerDef;
    public Fixture playerFixture;

    private Texture textur_player;
    private TmxMapLoader mapLoader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;

    private World world;
    private Box2DDebugRenderer b2db;

    private Stage stage;
    private float zoom = 0.05f;

    private Viewport gamePort;

    public PlayScreen(MyGdxGame game)
    {
        this.game=game;

        camera = new OrthographicCamera();

        gamePort= new FitViewport(MyGdxGame.V_WIDTH,MyGdxGame.V_HEIGHT,camera);
        hud=new Hud(game.batch);
        mapLoader=new TmxMapLoader();
        map = mapLoader.load("maps/m0.tmx");
        renderer = new OrthogonalTiledMapRenderer(map);

        //camera.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2, 0);
        world=new World(new Vector2(0,-100),true);
        camera.position.x = 4000;
        camera.position.y = 225;
        b2db=new Box2DDebugRenderer();

        player = new Player(this);
        BodyDef bodyDef= new BodyDef();
        PolygonShape shape=new PolygonShape();
        FixtureDef fixtureDef=new FixtureDef();
        Body body;


        //Creator .... bodies/fixtures

        //System.out.println(map.getLayers().get(3).getObjects().getByType(RectangleMapObject.class).size);

        for(MapObject object:map.getLayers().get(3).getObjects().getByType(RectangleMapObject.class))
        {
            //System.out.println("entro");
            Rectangle rect= ((RectangleMapObject)object).getRectangle();

            bodyDef.type=BodyDef.BodyType.StaticBody;
            bodyDef.position.set(rect.getX()+rect.getWidth()/2,rect.getY()+rect.getHeight()/2);
            body=world.createBody(bodyDef);

            shape.setAsBox(rect.getWidth()/2,rect.getHeight()/2);
            fixtureDef.shape=shape;
            body.createFixture(fixtureDef);
        }

        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                Fixture fixtureA = contact.getFixtureA();
                Fixture fixtureB = contact.getFixtureB();

                if (fixtureA.getUserData() instanceof Character){

                    Character _player = (Character) fixtureA.getUserData();

                    //System.out.println(_player.getName());
                    //System.out.println("Daño");
                    //_player.remove();
                    //_player.OnCollisionEnter(fixtureB.toString());
                }
            }

            @Override
            public void endContact(Contact contact) {
            }
            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {
            }
            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {
            }
        });

    }
    public void jump() {
        //Vector2 position = playerBody.getPosition();
        //playerBody.applyLinearImpulse(0,20,position.x,position.y,true);
        playerBody.applyLinearImpulse(new Vector2(0,4f),player.body.getWorldCenter(),true);
    }

    @Override
    public void show() {


    }
    public World getWorld(){
        return world;
    }
    public void handleInput(float dt){
        if(Gdx.input.isTouched())
            camera.position.x+=100*dt;
    }

    public void update(float dt){
        handleInput(dt);
        world.step(dt,6,2);

        camera.position.x=player.body.getPosition().x;
        camera.update();
        renderer.setView(camera);

    }



    @Override
    public void render(float delta) {

        update(delta);
        Gdx.gl.glClearColor(0.2f, 0.6f, 1.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);



        if (Gdx.input.isKeyJustPressed(Input.Keys.W))
            player.body.applyLinearImpulse(new Vector2(0,400f),player.body.getWorldCenter(),true);


        if (Gdx.input.isKeyJustPressed(Input.Keys.D))
            player.body.applyLinearImpulse(new Vector2(100f,0),player.body.getWorldCenter(),true);

        if (Gdx.input.isKeyJustPressed(Input.Keys.A) )
            player.body.applyLinearImpulse(new Vector2(-100f,0),player.body.getWorldCenter(),true);

        if (Gdx.input.isKeyJustPressed(Input.Keys.Z)) {
            camera.position.y+=100*delta;
        }


        renderer.render();
        b2db.render(world,camera.combined);
        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();
    }


    @Override
    public void resize(int width, int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        map.dispose();

        playerBody.destroyFixture(playerFixture);
        world.destroyBody(playerBody);
        renderer.dispose();
    }
}
