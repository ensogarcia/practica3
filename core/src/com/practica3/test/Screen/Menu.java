package com.practica3.test.Screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.practica3.test.MyGdxGame;
import com.practica3.test.Screen.BaseScreen;

public class Menu extends BaseScreen {

    public Menu(MyGdxGame game) {

        super(game);
        stage= new Stage();

        batch = new SpriteBatch();
        img = new Texture("playnow.png");
        suelo= new Texture("15.png");

        stage = new Stage(new FitViewport(640, 360));
        //skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        //retry = new TextButton("Play", skin);


        //retry.setSize(100, 30);
        //retry.setPosition(60, 50);

        //stage.addActor(retry);

    }

    private Stage stage;

    SpriteBatch batch;
    Texture img;
    private Texture suelo;

    private Skin skin;
    private TextButton retry;


    @Override
    public void show() {
        //Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.9f, 0.85f, 0.85f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        /*if (Gdx.input.isTouched()){
            //tamaño=Gdx.input.getX();
            System.out.print(Gdx.input.getX());
            System.out.println();
            System.out.print(Gdx.input.getY());
            System.out.println();
        }*/
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            System.out.println("adsadsadsadsadsa");
            //game.setScreen(new PlayGame(game));
        }
        //System.out.print(Gdx.input.getY());
        stage.act();

        batch.begin();
        batch.draw(suelo, 0f, 0f,800,450);
        batch.draw(img, 240, 100);
        batch.end();

        stage.draw();
    }
    @Override
    public void hide() {
        //Gdx.input.setInputProcessor(null);

    }

    public void dispose () {
        batch.dispose();
        img.dispose();
        stage.dispose();
    }

}
