package com.practica3.test;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.practica3.test.Screen.PlayScreen;

public class Player extends Sprite {
    public World world;
    public Body body;
    private PlayScreen screen;
    public Player(PlayScreen screen){
        this.screen = screen;
        this.world = screen.getWorld();
        definePlayer();
    }
    public void definePlayer(){
        BodyDef bodyDef=new BodyDef();
        bodyDef.position.set(30,30);
        bodyDef.type=BodyDef.BodyType.DynamicBody;
        body=world.createBody(bodyDef);

        FixtureDef fixtureDef=new FixtureDef();
        CircleShape shape= new CircleShape();
        shape.setRadius(10);
        System.out.println("SADSADSA");
        fixtureDef.shape=shape;
        body.createFixture(fixtureDef);
    }
}
